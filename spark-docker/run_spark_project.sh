#!/bin/bash
set -e

start-master.sh
start-slave.sh spark://spark:7077
echo "Running Spark Master and Slave"

spark-submit --class com.abhi.eventspipe.programs.CsvParsingProgram --master spark://spark:7077 /opt/jars/*.jar
spark-submit --class com.abhi.eventspipe.programs.SleepTrackerProgram --master spark://spark:7077 /opt/jars/*.jar
