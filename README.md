# Period and Sleep Events Project
by Abhijit Agarwal

#### Project Description:
- Parse the two CSV datasets
- Run some statistics on one of the dataset
- Merge two sleep datasets together
- Full Problem statement is in `Problem_Statement.md`

#### Pending
- Local mode setup: This application setup currently runs in local mode only, but the master can be changed with the param `--spark-master` (`local[*]` by default).
- Pipelining: Currently `spark-docker/run_spark_project.sh` is just a bunch of `spark-submit` jobs put together. They will always execute, and overwrite an already existing output each time. 
- No distributed storage option like HDFS or S3 is included

## Modules
### DatasetUtils
- `save()`: Save a Dataset as parquet (to disk)
- `saveAndLoad()`: Save a Dataset as parquet (to disk), and read it back again for continued processing
- `colsToUnixTimestamp`: Convert string columns to a UNIX timestamp (long)

## Programs
### CsvParsingProgram
This program CSV data from different sources, it parses them and then it stores this data in parquet.

##### Why Parquet?
- It has native support with Spark SQL
- It has faster serialization/deserialization than text formats like JSON, XML
- Snappy compression by default
- It has a schema with data types for attributes (String, Long, Date, even List and Map types.)
- Parquet is a columnar data structure. When using Spark SQL, only the columns that are needed for a job are deserialized

##### Config Params
Take a look at `src/main/resources/reference.conf`. These are the params available for `CsvParsingProgram`.
- `inputPath`: Path to input CSV file/folder
- `outputPath`: Path to output Parquet folder
- `maxFiles`: Maximum number of parquet files to create
- `partitionBy`: Optional. List of columns. If provided, data will be partitioned by value of these columns. 
- `datetimeCols`: Columns that contain a string datetime
- `datetimeFmt`: String format of `datetimeCols`

### SleepTrackerProgram
This program merges sleep data from `data_source_1.csv` and `data_source_2.csv` into one uniform dataset. It does not use raw CSV files anymore, but the processed parquet files from `CsvParsingProgram`.

##### Config Params
Take a look at `src/main/resources/reference.conf`. These are the params available for `CsvParsingProgram`.
- `categorical_data.input`: Data with sleep ranges (0-3, 3-6, etc.)
- `discrete_data.input`: Data with discrete values of `startTime` and `endTime`
- `discrete_data.output`: Processed discrete data with columns: `userId`, `date`, `sleepDuration`, `sleepRange`
- `statsJsonPath`: Path where we write sleep stats like max, min, percentile95
- `uniformOutputPath`: Path where the combined sleep dataset is stored
- `maxFiles`: Maximum number of parquet files to create


## How to run
- A "FAT" JAR file, `eventspipe/target/eventspipe-1.0-SNAPSHOT-jar-with-dependencies.jar`, has already been included in the git bundle. Normally I would not commit a binary to git, but I wanted to include a pre-compiled version of the project. 
- The two input CSV files, `input/data_source_{1,2}.csv` have also been included in the git bundle. Again, this is to save effort in running the project. 

### Running command

```
docker-compose build
docker-compose up
```

### Checking the output
Your output should look like this:

```
➜  eventsproject git:(master) ✗ tree output
output
├── README.txt
├── parquet
│   ├── data_source_1.parquet
│   │   ├── _SUCCESS
│   │   ├── type=period
│   │   │   ├── part-00000-5e087e2a-3758-464f-bc53-58cf18bc1076.c000.snappy.parquet
│   │   │   └── part-00001-5e087e2a-3758-464f-bc53-58cf18bc1076.c000.snappy.parquet
│   │   └── type=sleep
│   │       ├── part-00000-5e087e2a-3758-464f-bc53-58cf18bc1076.c000.snappy.parquet
│   │       └── part-00001-5e087e2a-3758-464f-bc53-58cf18bc1076.c000.snappy.parquet
│   ├── data_source_2.parquet
│   │   ├── _SUCCESS
│   │   ├── part-00000-3158e1c0-eb0e-4d98-a76b-9bfe1537d007-c000.snappy.parquet
│   │   └── part-00001-3158e1c0-eb0e-4d98-a76b-9bfe1537d007-c000.snappy.parquet
│   ├── sleep_processed_discrete.parquet
│   │   ├── _SUCCESS
│   │   ├── part-00000-a8c7555a-d3d2-49f8-8817-17951ac8da2d-c000.snappy.parquet
│   │   └── part-00001-a8c7555a-d3d2-49f8-8817-17951ac8da2d-c000.snappy.parquet
│   └── sleep_uniform.parquet
│       ├── _SUCCESS
│       ├── part-00000-83e5a182-2713-47f3-a2cc-5b6a270684fe-c000.snappy.parquet
│       ├── part-00001-83e5a182-2713-47f3-a2cc-5b6a270684fe-c000.snappy.parquet
│       ├── part-00002-83e5a182-2713-47f3-a2cc-5b6a270684fe-c000.snappy.parquet
│       ├── part-00003-83e5a182-2713-47f3-a2cc-5b6a270684fe-c000.snappy.parquet
│       └── part-00004-83e5a182-2713-47f3-a2cc-5b6a270684fe-c000.snappy.parquet
└── stats
    └── sleep_stats.json
        ├── _SUCCESS
        └── part-00000-eadc0b48-9651-4705-9962-4b5993730985-c000.json

9 directories, 20 files
```

You can also investigate the output in a spark-shell:

```
docker run -it --rm -v /<absolute_path>/eventsproject/output/:/data/output/ eventsproject_spark spark-shell
```

```
scala> spark.read.parquet("/data/output/parquet/sleep_uniform.parquet").show(5)
+--------------------+----------+----------------+----------------+-------------------+
|              userId|      date|oldSleepDuration|newSleepDuration|mergedSleepDuration|
+--------------------+----------+----------------+----------------+-------------------+
|00067b79-baeb-422...|2018-01-10|             0-3|            null|                0-3|
|000c7e2e-c134-472...|2018-01-20|             0-3|            null|                0-3|
|00113f86-34c8-4c2...|2018-01-20|            null|              >9|                 >9|
|00166f9b-431c-49c...|2018-01-12|            null|             3-6|                3-6|
|00232739-9b7c-48b...|2018-01-17|            null|             6-9|                6-9|
+--------------------+----------+----------------+----------------+-------------------+
only showing top 5 rows

```


### Changing the code or config
- If you change the code or `reference.conf`, you will need to recompile the FAT JAR. 

```
cd eventspipe
mvn clean compile assembly:single
```

- You can also add an additional config file `application.conf`, and include it in the Spark driver path. Then you will not need to recompile the JAR just for config changes. 


### Changing the run script
- If you change the run script, you will need to rebuild the docker image.
```
docker-compose build
```

## Problems Section
- Writing Junit tests for Spark Application proved to be a 7 hour ordeal. It was very complicated to come up with good ways of making Spark datasets, and there were several errors related to PowerMock and Hadoop lib that took a long time to solve.

- Ideally the data being consumed by the pipeline should already have UNIX timestamps in UTC, instead of String format with timezone.

- I made the assumption that every (userId, date) combination has only one event per dataset. This assumption could easily fail in real life systems. 


### Scaling the Application

To use such an application in production, several improvements would be needed:

- Adding distributed storage like S3 or HDFS
- Logging any data element that does not match our assumptions: endTime > startTime, values within range, etc.
- Data could be streamed instead of batch processed, to only process each new event once.
- To each data element, adding a time this event was recorded. An event recorded later takes preference over an event recorded earlier
- Another database technology could be utilized to keep track of the final merged sleep data: a key-value column store could be an example.
- Two different `sleepRange` columns should not be merged on write, but on read. The reader decides which dataset it prefers, that way if the second dataset proves unreliable, the data pipeline doesn't have to rewrite all results to updated the `mergedSleepRange` value
