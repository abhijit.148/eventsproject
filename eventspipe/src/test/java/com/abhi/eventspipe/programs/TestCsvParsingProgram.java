package com.abhi.eventspipe.programs;

import com.abhi.eventspipe.BaseTest;
import com.abhi.eventspipe.utils.DatasetUtils;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.mockito.Mockito;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DatasetUtils.class)
@PowerMockIgnore({"javax.management.*", "javax.security.auth.kerberos.*"})
public class TestCsvParsingProgram extends BaseTest {

    private CsvParsingProgram csvParser;
    private SparkSession spark;

    @Before
    public void setUp() {
        // Hadooplib issues: https://stackoverflow.com/questions/41864985/hadoop-ioexception-failure-to-login
        UserGroupInformation.setLoginUser(UserGroupInformation.createRemoteUser("junit"));
        spark = getSparkSession();
        PowerMockito.mockStatic(DatasetUtils.class);
        csvParser = new CsvParsingProgram();
    }

    /**
     * parseCsvToParquet is an I/O involved operation: it reads a CSV file, and
     * saves it as parquet; to test such a function we only test that given an
     * input path, it reads the input into a dataset and calls the save() method
     * in DatasetUtils.Class
     */
    @Test
    public void testParseCsvSaves() throws Exception {
        String testInput = "src/test/resources/test_data_source_1.csv";
        String[] partitionBy = {"col1"};
        Whitebox.invokeMethod(
                csvParser, "parseCsvToParquet", spark, testInput, "/output",
                null, null, 1, partitionBy);

        // Execute and verify that DatasetUtils.save was called
        PowerMockito.verifyStatic(DatasetUtils.class);
        DatasetUtils.save(Mockito.any(Dataset.class), Mockito.anyString(),
                Mockito.anyInt(), Mockito.any(String[].class));

    }

}
