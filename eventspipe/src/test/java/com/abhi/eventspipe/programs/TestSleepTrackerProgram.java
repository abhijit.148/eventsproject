package com.abhi.eventspipe.programs;

import com.abhi.eventspipe.BaseTest;
import com.abhi.eventspipe.utils.DatasetUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DatasetUtils.class)
// Powermock issues: https://github.com/powermock/powermock/issues/475
@PowerMockIgnore({"javax.management.*", "javax.security.auth.kerberos.*"})
public class TestSleepTrackerProgram extends BaseTest {

    private SleepTrackerProgram tracker;
    private SparkSession spark;

    @Before
    public void setUp() {
        // Hadooplib issues: https://stackoverflow.com/questions/41864985/hadoop-ioexception-failure-to-login
        UserGroupInformation.setLoginUser(UserGroupInformation.createRemoteUser("junit"));
        spark = getSparkSession();
        PowerMockito.mockStatic(DatasetUtils.class);
        tracker = new SleepTrackerProgram();
    }

    @Test
    public void testProcessDiscreteData() throws Exception {
        StructField[] structFields = new StructField[]{
            new StructField("userId", DataTypes.StringType, true, Metadata.empty()),
            new StructField("startTime", DataTypes.LongType, true, Metadata.empty()),
            new StructField("endTime", DataTypes.LongType, true, Metadata.empty())
        };
        StructType structType = new StructType(structFields);

        List<Row> rows = new ArrayList<>();
        rows.add(RowFactory.create("case1", 1516827480l, 1516848900l));
        // This row should be filtered out because it has no startTime
        rows.add(RowFactory.create("case2", null, 1516848900l));
        // This row should be filtered out because it has no endTime
        rows.add(RowFactory.create("case3", 1516827480l, null));
        // This row should be filtered out because endTime > startTime
        rows.add(RowFactory.create("case4", 1516848900l, 1516827480l));

        Dataset<Row> ds = getSparkSession().createDataFrame(rows, structType);

        Dataset<Row> processed = Whitebox.invokeMethod(tracker, "processDiscreteData", ds);

        List<Row> results = processed
                // Make sure required columns are present
                .select("userId", "date", "sleepDuration", "sleepRange")
                .collectAsList();
        // Results should only contain 1 row
        Assert.assertEquals(1, results.size());

        // processed.printSchema();
        Row result = results.get(0);
        // Check date
        Assert.assertEquals("2018-01-25", result.getString(1));
        // Check sleepDuration
        Assert.assertEquals(5.95, result.getDouble(2), 0.02);
        // Check sleepRange
        Assert.assertEquals("3-6", result.getString(3));
    }

    @Test
    public void testGetSleepStats() throws Exception {
        // This CSV columns has userId (str), date(str), sleepDuration(long), sleepRange(str)
        Dataset<Row> discrete = readTestCsv(
                spark, "src/test/resources/test_discrete_processed.csv");

        Dataset<Row> processed = Whitebox.invokeMethod(tracker, "getSleepStats", discrete);

        List<Row> results = processed
                // Make sure required columns are present
                .select("minSleep", "maxSleep", "percentile95")
                .collectAsList();
        // Results should only contain 1 row
        Assert.assertEquals(1, results.size());

        Row result = results.get(0);
        // Check minSleep
        Assert.assertEquals(5.95, result.getDouble(0), 0.01);
        // Check maxSleep
        Assert.assertEquals(9.0, result.getDouble(1), 0.01);
        // Check 95 percentile
        Assert.assertEquals(9.0, result.getDouble(2), 0.01);
    }

    @Test
    public void testMergeSleepData() throws Exception {
        Dataset<Row> categorical = readTestCsv(
                spark, "src/test/resources/test_data_source_1.csv");

        // This CSV columns has userId (str), date(str), sleepDuration(long), sleepRange(str)
        Dataset<Row> discrete = readTestCsv(
                spark, "src/test/resources/test_discrete_processed.csv");

        Dataset<Row> processed = Whitebox.invokeMethod(
                tracker, "mergeSleepData", categorical, discrete);

        List<Row> results = processed
                // Make sure required columns are present
                .select("userId", "date", "mergedSleepDuration")
                // Sort by userId, date for reproducible order of results
                .sort("userId", "date")
                .collectAsList();

        /*
        * Results should only contain 4 rows:
        * 0 for user1, 1 for user2, 1 for user3, 2 for user4
         */
        Assert.assertEquals(4, results.size());

        Row mergedRow;

        mergedRow = results.get(0);
        // User2 is only in data_source_1
        Assert.assertEquals("3-6", mergedRow.getString(2));

        mergedRow = results.get(1);
        // User 3 is in both, check that discrete overrides categorical
        Assert.assertEquals("3-6", mergedRow.getString(2));

        mergedRow = results.get(2);
        // User 4 is only in discrete
        Assert.assertEquals("6-9", mergedRow.getString(2));
    }

}
