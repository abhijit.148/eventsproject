package com.abhi.eventspipe.utils;

import com.abhi.eventspipe.BaseTest;
import java.util.ArrayList;
import java.util.List;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.Assert;
import org.junit.Test;
import org.spark_project.guava.collect.Lists;

public class TestDatasetUtils extends BaseTest {

    /**
     * This test checks that a datetime string is correctly converted to a UTC
     * Unix timestamp (long value)
     */
    @Test
    public void testColsToUnixTimestamp() {
        StructField[] structFields = new StructField[]{
            new StructField("dateString", DataTypes.StringType, true, Metadata.empty())
        };
        StructType structType = new StructType(structFields);

        List<Row> rows = new ArrayList<>();
        rows.add(RowFactory.create("2018-01-20T22:03:00.000+01:00"));

        Dataset<Row> ds = getSparkSession().createDataFrame(rows, structType);

        ds = DatasetUtils.colsToUnixTimestamp(
                ds, Lists.newArrayList("dateString"), "yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

        long actual = ds.collectAsList().get(0).getLong(0);
        long expected = 1516482180l; // Timestamp in UTC
        Assert.assertEquals(expected, actual);
    }

}
