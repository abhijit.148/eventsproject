package com.abhi.eventspipe;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * Base class for a Test
 */
public abstract class BaseTest {

    protected SparkSession getSparkSession() {
        SparkConf sparkConf = new SparkConf().setAppName(this.getClass().toString());
        sparkConf.setMaster("local[*]");
        sparkConf.set("spark.sql.warehouse.dir", "/tmp/spark-warehouse-dir");

        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();
        return spark;
    }

    protected Dataset<Row> readTestCsv(SparkSession spark, String path) {
        return spark
                .read()
                .option("header", "true")
                .option("comment", "#")
                .option("inferSchema", "true")
                .csv(path);
    }
}
