package com.abhi.eventspipe.programs;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

/**
 * Base class for all Spark Programs
 */
public abstract class BaseProgram {

    @Parameter(names = {"-h", "--help"}, description = "Show the arguments of this program.")
    protected boolean help;
    
    @Parameter(names = {"--spark-master"}, description = "Spark Master address")
    protected String sparkMaster = "local[*]";

    public abstract void run() throws Exception;
    
    // This is the config block the program will have available
    public abstract String getConfigName();
    
    public abstract String getName();

    public abstract String getDescription();

    public boolean isHelp() {
        return help;
    }

    public static int run(Class<? extends BaseProgram> program, String[] args) {
        try {
            BaseProgram pgr = program.newInstance();
            JCommander jComm = new JCommander(pgr);
            jComm.setProgramName(pgr.getName() + " - " + pgr.getDescription());
            try {
                jComm.parse(args);
                if (pgr.isHelp()) {
                    jComm.usage();
                    return 0;
                }
            } catch (Throwable t) {
                t.printStackTrace();
                jComm.usage();
                return -1;
            }

            return run(pgr, args);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static int run(BaseProgram pgr, String[] args) {
        try {
            pgr.run();
            return 0;
        } catch (Throwable t) {
            t.printStackTrace();
            return -1;
        }
    }
    
    protected SparkSession getSparkSession() {
        SparkConf sparkConf = new SparkConf().setAppName(getName());
        sparkConf.setMaster(sparkMaster);

        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();
        return spark;
    }
    
    protected Config getProgConfig() {
        return ConfigFactory.load().getConfig(getConfigName());
    }
    
}
