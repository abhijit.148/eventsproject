package com.abhi.eventspipe.programs;

import java.util.List;
import com.typesafe.config.Config;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static com.abhi.eventspipe.utils.ConfigUtils.*;
import static com.abhi.eventspipe.utils.DatasetUtils.*;

/**
 * Class to Parse CSV files into Parquet Files. It is a configurable program
 * that is controlled by src/main/resources/reference.conf
 *
 * You can adjust the config file to adjust the number of sources, input paths,
 * output paths, max number of output files, and partitioning columns.
 */
public class CsvParsingProgram extends BaseProgram {

    public static void main(String[] args) {
        BaseProgram.run(CsvParsingProgram.class, args);
    }

    @Override
    public void run() throws Exception {
        SparkSession spark = getSparkSession();
        Config progConfig = getProgConfig();

        progConfig.getConfigList("sources").forEach((sourceConfig) -> {

            parseCsvToParquet(
                    spark,
                    sourceConfig.getString("inputPath"),
                    sourceConfig.getString("outputPath"),
                    getStringListOrNull(sourceConfig, "datetimeCols"),
                    getStringOrNull(sourceConfig, "datetimeFmt"),
                    sourceConfig.getInt("maxFiles"),
                    getStringArrOrNull(sourceConfig, "partitionBy")
            );

        });

    }

    /**
     * Program to parse CSV from input and write it to output, optionally
     * partitioned
     *
     * @param spark SparkSession
     * @param input Path to CSV File(s)
     * @param output Path to Parquet folder
     * @param maxFiles Maximum number of Output Parquet Files
     * @param partitionByArr Optional, columns to partition data on
     */
    private void parseCsvToParquet(SparkSession spark, String input, String output,
            List<String> datetimeCols, String datetimeFmt, int maxFiles, String[] partitionBy) {
        Dataset<Row> ds = spark
                .read()
                .option("header", "true")
                .csv(input);

        // Replace string datetime columns with UTC timestamp columns
        if (datetimeCols != null && datetimeFmt != null) {
            ds = colsToUnixTimestamp(ds, datetimeCols, datetimeFmt);
        }

        save(ds, output, maxFiles, partitionBy);
    }

    @Override
    public String getConfigName() {
        return "csvprogram";
    }

    @Override
    public String getName() {
        return "CSV Parsing Program";
    }

    @Override
    public String getDescription() {
        return "Program to parse a CSV file into a Parquet file";
    }
}
