package com.abhi.eventspipe.programs;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;
import static com.abhi.eventspipe.utils.ConfigUtils.*;
import static com.abhi.eventspipe.utils.DatasetUtils.saveAndLoad;
import static com.abhi.eventspipe.utils.ScalaUtils.stringListToSeq;
import com.typesafe.config.Config;
import java.util.List;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.DataTypes;
import org.spark_project.guava.collect.Lists;

/**
 * Class to Merge sleeping data from a dataset with categorical duration and
 * another dataset with discrete value durations. It is a configurable program
 * that is controlled by src/main/resources/reference.conf
 *
 * You can adjust the config file to adjust the number of sources, input paths,
 * output paths, max number of output files, and partitioning columns.
 */
public class SleepTrackerProgram extends BaseProgram {

    public static void main(String[] args) {
        BaseProgram.run(SleepTrackerProgram.class, args);
    }

    @Override
    public void run() throws Exception {
        SparkSession spark = getSparkSession();
        Config progConfig = getProgConfig();
        int maxFiles = progConfig.getInt("maxFiles");

        // Data with discrete values for startTime and Endtime
        String[] discretePaths = getStringArrOrNull(progConfig, "discrete_data.inputPath");
        Dataset<Row> discreteData = spark.read().parquet(discretePaths);

        // Add a column for sleep range (category values)
        discreteData = processDiscreteData(discreteData);

        // Checkpoint. Write data to parquet and read again
        discreteData = saveAndLoad(
                discreteData,
                progConfig.getString("discrete_data.outputPath"),
                maxFiles
        );

        // Write stats from discrete data: avg min, avg max, avg 95 percentile
        getSleepStats(discreteData)
                .coalesce(1)
                .write()
                .mode(SaveMode.Overwrite)
                .json(progConfig.getString("statsJsonPath"));

        // Data with category values (0-3, 3-6, 6-9, >9)
        String[] categoryPaths = getStringArrOrNull(progConfig, "categorical_data.inputPath");
        Dataset<Row> categoryData = spark.read().parquet(categoryPaths);

        // Merge both datasets and write a uniform dataset
        mergeSleepData(categoryData, discreteData)
                .coalesce(maxFiles)
                .write()
                .mode(SaveMode.Overwrite)
                .parquet(progConfig.getString("uniformOutputPath"));
    }

    /**
     * A method to parse a newer, discrete value dataset. These discrete sleep
     * durations are parsed to a range column (0-3, 3-6, etc.)
     *
     * @param discreteData userId, startTime, endTime
     * @return Dataset with userId, date, sleepDuration, sleepRange
     */
    private Dataset<Row> processDiscreteData(Dataset<Row> discreteData) {
        UserDefinedFunction durationToRange = udf(
                (Double dur) -> {
                    if (dur <= 3) {
                        return "0-3";
                    }
                    if (dur <= 6) {
                        return "3-6";
                    }
                    if (dur <= 9) {
                        return "6-9";
                    }
                    return ">9";
                }, DataTypes.StringType
        );

        Dataset<Row> sleepDataFromDiscreet = discreteData
                // TODO: log invalid events
                .filter("endTime > 0 AND startTime > 0")
                // Date is the date the person woke up (endTime), as string
                .withColumn("date", from_unixtime(col("endTime"), "yyyy-MM-dd"))
                // Get number of hours from seconds
                .withColumn("sleepDuration", expr("(endTime - startTime) / 60 / 60"))
                // TODO: log negative duration events
                .filter("sleepDuration > 0")
                .withColumn("sleepRange", durationToRange.apply(col("sleepDuration")))
                .select("userId", "date", "sleepDuration", "sleepRange");

        return sleepDataFromDiscreet;
    }

    /**
     * Get sleep statistics from the newer, discrete value sleep dataset. For
     * each user, get the average hours of sleep. And then calculate statistics
     * on these averages.
     *
     * @param discreteData userId, date, sleepDuration, sleepRange
     * @return Dataset with columns maxSleep, minSleep, percentile95
     */
    private Dataset<Row> getSleepStats(Dataset<Row> discreteData) {
        Dataset<Row> avgSleep = discreteData
                .groupBy("userId")
                .agg(avg(col("sleepDuration")).as("avgSleep"));

        return avgSleep.agg(
                min(col("avgSleep")).as("minSleep"),
                max(col("avgSleep")).as("maxSleep"),
                callUDF("approx_percentile", col("avgSleep"), lit(0.95)).as("percentile95")
        );
    }

    /**
     * Merge the current category value sleep dataset with newer discrete value
     * dataset. Discrete value dataset is preferred over category value in case
     * of a conflict.
     *
     * @param categoryData Sleep dataset with categorical values. Required cols:
     * userId, date, type, value
     * @param discreteData new Sleep dataset with discrete values Required cols:
     * userId, date, sleepRange
     * @return Dataset with cols userId, date, sleepRange
     */
    private Dataset<Row> mergeSleepData(Dataset<Row> categoryData, Dataset<Row> discreteData) {
        List<String> joinCols = Lists.newArrayList("userId", "date");

        Dataset<Row> merged = categoryData
                // Get only sleep data (not period data)
                .filter(col("type").equalTo("sleep"))
                .selectExpr("userId", "date", "value")
                // Join with discrete data
                .join(discreteData, stringListToSeq(joinCols), "outer");

        merged = merged
                .selectExpr(
                        "userId", "date",
                        "value as oldSleepDuration",
                        "sleepRange as newSleepDuration")
                // mergedSleepRange = sleepRange OR value
                .withColumn("mergedSleepDuration", expr("coalesce(newSleepDuration, oldSleepDuration)"));

        return merged;
    }

    @Override
    public String getConfigName() {
        return "sleeptracker";
    }

    @Override
    public String getName() {
        return "Sleep Tracker Program";
    }

    @Override
    public String getDescription() {
        return "Merges a categorial dataset and a discrete value dataset into one sleep dataset";
    }

}
