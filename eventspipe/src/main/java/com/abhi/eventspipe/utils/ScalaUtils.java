/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abhi.eventspipe.utils;

import java.util.List;
import scala.collection.JavaConverters;
import scala.collection.Seq;

/**
 *
 * @author abhijit
 */
public class ScalaUtils {
    
    public static Seq<String> stringListToSeq(List<String> l) {
        return JavaConverters.asScalaIteratorConverter(l.iterator()).asScala().toSeq();
    }
    
}
