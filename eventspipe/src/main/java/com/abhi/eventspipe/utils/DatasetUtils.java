package com.abhi.eventspipe.utils;

import java.util.List;
import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.unix_timestamp;

public class DatasetUtils {

    /**
     * Save a dataset as parquet to a path with maxFiles, optionally
     * partitioning by column(s)
     *
     * @param ds
     * @param path
     * @param maxFiles
     * @param partitionBy Optional
     */
    public static void save(
            Dataset<Row> ds, String path, int maxFiles, String[] partitionBy) {
        DataFrameWriter<Row> writer = ds
                .coalesce(maxFiles)
                .write()
                .mode(SaveMode.Overwrite);

        // Save data partitioned by specific column(s)
        if (partitionBy != null) {
            writer = writer.partitionBy(partitionBy);
        }
        writer.parquet(path);
    }

    public static void save(Dataset<Row> ds, String path, int maxFiles) {
        save(ds, path, maxFiles, null);
    }

    /**
     * Save to disk in parquet and load dataset again, for checkpointing
     * purposes.
     *
     * @param ds
     * @param path
     * @param maxFiles
     * @param partitionBy Optional
     * @return The same dataset, but saved and read from Disk
     */
    public static Dataset<Row> saveAndLoad(
            Dataset<Row> ds, String path, int maxFiles, String[] partitionBy) {
        SparkSession spark = ds.sparkSession();

        // Write to Disk
        save(ds, path, maxFiles, partitionBy);

        // Read back again
        ds = spark.read().parquet(path);

        return ds;
    }

    public static Dataset<Row> saveAndLoad(Dataset<Row> ds, String path, int maxFiles) {
        return saveAndLoad(ds, path, maxFiles, null);
    }

    /**
     * Convert string datetime columns to a UTC timestamp
     *
     * @param ds
     * @param datetimeCols List of datetime column names
     * @param datetimeFmt String format for the datetime column
     * @return Dataset with datetime columns converted
     */
    public static Dataset<Row> colsToUnixTimestamp(
            Dataset<Row> ds, List<String> datetimeCols, String datetimeFmt) {
        for (String dcol : datetimeCols) {
            // Convert String date to Unix timestamp (seconds) in UTC
            ds = ds.withColumn(dcol, unix_timestamp(col(dcol), datetimeFmt));
        }
        return ds;
    }

}
