/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.abhi.eventspipe.utils;

import com.typesafe.config.Config;
import java.util.List;


public class ConfigUtils {
    public static String getStringOrNull(Config config, String key) {
        if (config.hasPath(key)) {
            return config.getString(key);
        }
        return null;
    }
    
    public static List<String> getStringListOrNull(Config config, String key) {
        if (config.hasPath(key)) {
            return config.getStringList(key);
        }
        return null;
    }
    
    public static String[] getStringArrOrNull(Config config, String key) {
        if (config.hasPath(key)) {
            List<String> l = config.getStringList(key);
            String[] arr = l.toArray(new String[l.size()]);
            return arr;
        }
        return null;
    }
    
}
