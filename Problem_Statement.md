# Data engineer challenge

## Objective
We already have a system that receives sleep and period measurements from a data source. We want to integrate a different data source tha provides only sleep measurements, but in a different format and granularity.

## Purpose
This challenge will be used to:
* evaluate your ability to think and design a solution
* evaluate your technical experience and coding abilities

## Grading
The challenge will be evaluated on:
* proposed solution its documentation
* appropriate use of git
* testing
* coding standards
* documentation around possible improvements or different approaches

## Context and data sets
Existing systems are already receiving and storing data from `data_source_1`.
A sample data set is provided as a compressed `data_source_1.tar.gz`.
The structure is the following:
- `userId`: an id to uniquely identify users
- `date`: date in ISO 8601 format. In case of sleep it refers to the day in which the user woke up.
- `type`: describes the type of measurement that is being recorded
- `value`: it differs based on `sleep` and `period` types
  - when type is `sleep` the values are represented in hours
  - when type is `period` possible values are `heavy`, `medium`, `light`

We want to integrate a new data source, called `data_source_2`. Sample data are provided in a compressed archive `data_source_2.tar.gz`. This data source provides only data about sleep, structured in the following way:
- `userId`: an id to uniquely identify users
- `startTime`: the time when the user started a sleep phase
- `endTime`: the time when the user ended a sleep phase

It may be possible that we get measurements for the same user but from different data sources, when this happens the measurement from `data_source_2` have the precedence since it has a bigger granularity and is considered more reliable.
Current implemented APIs expect to receive back only sleep range measurements.

## Task
Your application will:
* parse the provided sample data-files
* for each `data_source_2` event calculate the sleep range
* provide insights about the avg sleep minimum, maximum and 95 percentile of the average.
* store the results for further processing
You should:
* point out problems that occurred during development
* illustrate how your application can scale or what should be changed in order to meet scalability needs
* document how to run the project and what to expect when running it

The deliverable is compressed git repo with the code that implements the aforementioned points along with documentation that describes your assumptions and solution.

## Environment and language
You have a free hand in choosing the environment, tooling or libraries in which this code is developed and demonstrated.
The program should be developed either in Python, Java or Kotlin.
